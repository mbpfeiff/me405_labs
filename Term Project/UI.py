# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 18:58:01 2021

@author: matth
"""

import serial
import keyboard

ser = serial.Serial(port='COM5', baudrate=115273, timeout=1)
pushed_key = None
state = 0

def on_keypress (thing):
    
    """ 
    @brief Callback which runs when the user presses a key.
    
    @details This function is run as an interrupt whenever the user presses a 
    key. This enables user input without using the non-cooperative input 
    command.
    
    @param thing Keypstroke content to be stored for Vendotron to identify.
    
    """
    global pushed_key

    pushed_key = thing.name

def InitMessage():
    print('''
          
Press [G] to begin...
          
          ''')
    
while True:
    try:
        if state == 0:
            keyboard.on_press(on_keypress)
            state = 1
            newK = []
            InitMessage()

        elif state == 1:
            if pushed_key == 'G':
                pushed_key = None
                ser.write(str('G').encode('ascii'))
                myval = ser.readline().decode('ascii')
                print(myval)
                state = 2
            else:
                pushed_key = None
        elif state == 2:   
            Myval = ser.readline().decode('ascii')
            if Myval != '':
                print(Myval)
                
            if pushed_key != None:
                if pushed_key == '1':
                    ser.write(str('1').encode('ascii'))
                    print(str(ser.readline().decode('ascii')))
                    pushed_key = None
                    state = 3
                elif pushed_key == '2':                
                    ser.write(str('2').encode('ascii'))
                    print(str(ser.readline().decode('ascii')))
                    pushed_key = None
                    state = 3
                elif pushed_key == '3':
                    ser.write(str('3').encode('ascii'))
                    print(str(ser.readline().decode('ascii')))
                    pushed_key = None
                    state = 3
                elif pushed_key == '4':                
                    ser.write(str('4').encode('ascii'))
                    print(str(ser.readline().decode('ascii')))
                    pushed_key = None
                    state = 3
                else:
                    pushed_key = None
                    
        elif state == 3:
            
            if pushed_key != None:   
                print(str(pushed_key))
                if pushed_key == 'enter':
                    state = 4   
                newK.append(str(pushed_key))
                print(newK)
                pushed_key = None  
                
        elif state == 4:
            print(newK)
            for val in range(len(newK)):
                print('gets here')
                ser.write(str(newK[val]).encode('ascii'))
                print('heres the prob')
            newK = []
            pushed_key = None
            state = 2
    except:
        ser.close()
        break