#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
@file main.py

@brief    This file uses the cotask.py and task_share.py files to create 
          a scheduler for the ball-balancing controller.
@details  The main purpose of this code is use the FindPos, MotorDriver, and encoder
          drivers to create a controller for balacing a ball on a platform. The 
          platform is composed of two separate motors and encoder. The goal is
          to create a method for obtaining the ball state at any given time. 
          Another method that is in charge of obtaining the platforms state. 
          And a last method which has the duty of commanding the motors motions
          relative to the motion of the ball. Please read each individual method
          for detail function. This main file also works along with cotask.py and
          task_share.py which are imported from JR Ridgely BitBucket sourcecode.
          
@author Matthew Pfeiffer
@author Adan Martinez
@date Mar. 15, 2021
'''
import pyb
from pyb import UART
from micropython import const, alloc_emergency_exception_buf
import gc
import utime

import cotask
import task_share

import FindPos
import MotorDriver
import encoder

global BallPos
global PlatPos
global MotorCont

global k1
global k2
global k3
global k4

global Interval

## Sets the value of the controller gains
k1 = -0.000 
k2 = -8.4
k3 = -.525
k4 = .05

myuart = UART(2)
    
alloc_emergency_exception_buf (100)

def Ball_state_fun ():
    '''
    @brief      The Ball_state_fun function is responsible for obtaining the ball position
    @details    Ball_state_fun function uses the FindPos.py driver to create a driver for 
                the touchscreen on to of the platform. This touchscreen is then scanned
                in the x, y, and z axes all at once and the value is placed in a queue
                used to calculate the torque input necessary.
    '''
    state = 0
        
    while True:
        PlatPos.go()
        if state == 0:
            ## Runs the initial state
            
            ## Sets up the TocuhControl from the FindPos module.
            TouchScreen = FindPos.TouchControl(pyb.Pin.cpu.A6, pyb.Pin.cpu.A0, 
                                               pyb.Pin.cpu.A7, pyb.Pin.cpu.A1,
                                               176, 100, (176/2, 100/2))
            state = 1  # Sets the next state.
        elif state == 1:
            ## Runs state 1
            
            if not Ball_Info.full ():
                zact = []
                for idx in range(3):
                    BallPosition = TouchScreen.scanall()
                    xrange = BallPosition[0]
                    yrange = BallPosition[1]
                    zact.append(BallPosition[2])
                    
                if zact[0] == 0:
                    if zact[1] == 0:
                        if zact[2] == 0:
                            zactive = 0
                else: zactive = 1        
                    
                Ball_Info.put(BallPosition[0])
                Ball_Info.put(BallPosition[1])
                Ball_Info.put(zactive)
                Ball_Info.put(utime.ticks_us())
            else:
                #print('ball info full')
                pass
        yield (state)
        
def Platf_state_fun ():  
    '''
    @brief       A method that check the state of the platform. 
    @details     This method uses the EncoderDriver class to check the state of
                 the platform. The way the encoder are set up are use timer 4
                 and 8 to reach from two separate encoders. The method is consists
                 of two states. First, it sets up the EncoderDrivers. Next state,
                 it gets the angle and speed from both encoders and saves them
                 into the task_share file. 
    '''
    ## Initial state.
    state = 0
    while True:
        MotorCont.go()
        if state == 0:
            ## Runs the first state.
            # Sets up the first encoder with timer 4, pin B6, pin B7, and a 10 second interval.
            ENCx = encoder.EncoderDriver( 4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7,10)
            # Sets up the second encoder with timer 8, pin C6, pin C7, and a 10 second interval.
            ENCy = encoder.EncoderDriver( 8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7,10)
            state = 1    # Next state to run.          
        elif state == 1:
            ## Runs the second state.
            
            ## Only runs if Pos_Info is not full.
            if not Pos_Info.full ():
                ## Get the angle and speed reading from both encoders and saves it 
                #  into Pos_Info
                Pos_Info.put((50/60)*ENCx.get_angle ())
                Pos_Info.put((50/60)*ENCy.get_angle ())
            ## Only runs if Speed_Info is not full.   
            if not Speed_Info.full ():
                Speed_Info.put((50/60)*ENCx.get_speed ())
                Speed_Info.put((50/60)*ENCy.get_speed ())

      
        yield (state)    
        
def Motor_fun ():
    '''
    @brief     A method that comands the motion of the motor depending on the 
               platform and ball state.
    @details   This method consist of a FSM running in a while loop. The initial 
               state is initialize the pins for the motors control. The gain 
               values  (k1, k2, k3, k4) need to be specify here. The next state 
               had the functionality to retrieve the data save in the task_share. 
               If there is not data yet, the program will continue running in a 
               while loop. In state 2, the program calculate the torque needed
               to balance the system. It then calculates and sets the necessary 
               duty cycle. It then enables both the motors and runs them. Then,
               it goes back to state 1 and continues the cycle.
    '''
    state = 0

    while True:
        BallPos.go()
        if state == 0:
            ## Runs state 0
            ## Create the pin objects used for interfacing with the motor driver
            pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)   
            pin_nFAULT  = pyb.Pin(pyb.Pin.board.PB2)
            pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4) 
            pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5)
            pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)
            pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1) 
            
            ## Sets up and initializes the nFault pin as B15
            nfaultset = pyb.Pin(pyb.Pin.cpu.B15)
            nfaultset.init(mode=pyb.Pin.OUT_PP, value=0)
            
            ## Sets value a channel for the motor.
            ch1 = 1
            ch2 = 2
            ch3 = 3
            ch4 = 4
            
            # Create the timer object used for PWM generation
            timer = pyb.Timer(3, freq=20000)
            # Sets up MotorDriver class with the appropiate inputs.
            moex = MotorDriver.MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, pin_IN2, ch1, ch2, timer)
            moey = MotorDriver.MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN3, pin_IN4, ch3, ch4, timer)
            

            ## Sets the inital positions to zero.
            good_x = 0
            good_y = 0
            R = 2.21 #[ohms]
            Vdc = 12 #[Volts]
            Kt = 13.8 #[milli-Newton meters per Amps]
            Interval = False
            Dutyx = False
            Dutyy = False
            Dutyx_form = False
            Dutyy_form = False
            
            
            x_speed = 0
            y_speed = 0
            
            stime = utime.ticks_us()
            state = 1   ## Next state to run.
            
            
        elif state == 1:
            ## Runs state 1
            if Ball_Info.any ():
                ## Runs only if Ball_Info has content.
                x_pos = Ball_Info.get()  # Gets the x position from the Ball_Info data
                y_pos = Ball_Info.get()  # Gets the y position from the Ball_Info data
                z_pos = Ball_Info.get()  # Gets the z position from the Ball_Info data

                endtime = Ball_Info.get()    
                Interval = utime.ticks_diff(endtime,stime)  
                stime = endtime

                if int(z_pos) == 0:
                    ## Only runs if the ball is not touching the touch panel.
                    if good_x != False:
                        if good_x != 0:
                            # Only runs if the x position is not equal to False.
                            x_pos_former = good_x    #Sets the good x equal to the x_pos_former                      
                            good_x = x_pos   # Sets the good_x value equal to the x position
                            if Interval != False:
                                x_speed = (good_x-x_pos_former)/(Interval*(1e-6))  #Calculates the difference of the x positions and divides by the interval.
                            else:
                                x_speed = 0
                    else:
                        good_x = x_pos
                        x_speed = 0
                    if good_y != False:
                        ## Check that the value from the y position and calculates the 
                        #  speed of change of the y position. 
                        if good_y != 0:
                            y_pos_former = good_y                        
                            good_y = y_pos
                            if Interval != False:
                                y_speed = (good_y-y_pos_former)/((Interval)*(1e-6))
                            else:
                                y_speed = 0
                    else:
                        good_y = y_pos
                        y_speed = 0
                else:
                    ## Only runs if the position of the ball is at the center of the platform.                            
                    good_x = False
                    good_y = False 
                    x_speed = 0
                    y_seed = 0
                
            if Pos_Info.any ():
                ## Only runs when the Pos_Info has content. 
                theta_x = Pos_Info.get ()
                theta_y = Pos_Info.get ()
            if Speed_Info.any (): 
                ## Pulls the angular and linear acceleration data from Speed_Info.
                theta_dot_x = Speed_Info.get ()
                theta_dot_y = Speed_Info.get ()
            else:
                ## Sets the parameters back to zero. 
                theta_y = 0
                theta_x = 0
                theta_dot_y = 0
                theta_dot_x = 0
                
            if Dutyx != False:
                Dutyx_form = Dutyx
                
            if Dutyy != False:
                Dutyy_form = Dutyy

            Torque_x = ( - k4*theta_dot_x - k2*theta_x - k3*good_y -k1*y_speed  ) # [N-mm]             
            Torque_y = (- k4*theta_dot_y - k2*theta_y - k3*good_x -k1*x_speed ) # [N-mm]           
            Dutyx = -(R*100/(Vdc*Kt))*Torque_x
            Dutyy = -(R*100/(Vdc*Kt))*Torque_y
            
            if Dutyx_form != False:
                if Dutyx > Dutyx_form + 5:
                    Dutyx = (Dutyx + Dutyx_form)/2
                if Dutyx < Dutyx_form - 5:
                    Dutyx = (Dutyx + Dutyx_form)/2
            if Dutyy_form != False:
                if Dutyy > Dutyy_form + 5:
                    Dutyy = (Dutyy + Dutyy_form)/2
                if Dutyx < Dutyx_form - 5:
                    Dutyy = (Dutyy + Dutyy_form)/2
            if Dutyx > 50:
                Dutyx = 50
            elif Dutyx < -50:
                Dutyx = -50
            if Dutyy > 50:
                Dutyy = 50
            elif Dutyy < -50:
                Dutyy = -50
            ## Enables the motor 1
            moex.enable()            
            moex.set_duty(Dutyx)  # Sets the duty cycle
            # Enables the second motor
            moey.enable()
            moey.set_duty(Dutyy)      #Enables motor 2         
        yield(state)



# def UI_fun ():  
    # '''
    # @brief A user interface task that is still in development
    # @details This User interface task below was in development to provide a simple way
    # of initializing the system, changing the gain values, and passing data from the system
    # to a frontend UI for visualization of data. This was unfortunately not completed by the
    # due date of the term project and is therefore unimplemented in this code.    
    # '''
    # state = 0

    
    # while True:
        # if myuart.any() != 0:
            # val = myuart.readchar()
            # myuart.write(b'initializing...\n\n')
            # if val == 71:
                # break
    
    # while True:
        # BallPos.go()
        # if state == 0:
            # Buffer = []
            # state = 1
            
        # elif state == 1:
            # myuart.write('Press [1], [2], [3], or [4] to change\n')
            # myuart.write('k1 = ' + str(k1) + '\n' +
                         # 'k2 = ' + str(k2) + '\n' +
                         # 'k3 = ' + str(k3) + '\n' +
                         # 'k4 = ' + str(k4) + '\n')
            # state = 2
        # elif state == 2:
            # if myuart.any() != 0:
                # val = myuart.readchar()
                # if val == 49:
                    # myuart.write('Enter new k1\n')
                    # state = 3
                # elif val == 50:
                    # myuart.write('Enter new k2\n')
                    # state = 4
                # elif val == 51:
                    # myuart.write('Enter new k3\n')
                    # state = 5
                # elif val == 52:
                    # myuart.write('Enter new k4\n')
                    # state = 6
        # elif state == 3:
            # if myuart.any() != 0:
                # val = myuart.readchar()
                # if val == 10:
                    # state = 7
                # else:
                    # Buffer.append(val)

        # elif state == 7:
            # print(str(Buffer))
            # dec_pos = 0
            # num = 0
            # for n in Buffer:
                # if dec_trig == True:
                    # dec_pos += 1
                    # Buffer[n] = Buffer[n] - 0x30
                    # num = num + Buffer[n]/10*dec_pos
                # elif Buffer[n] == 0x2E:
                    # dec_trig = True
                # else:
                    # Buffer[n] = Buffer[n] - 0x30                        
                    # num = num*10 + Buffer[n]
            # print(str(num))
            # state = 1       
                                
        # # elif state == 4:
            # # if myuart.any() != 0:
                # # val = myuart.readchar()
        # # elif state == 5:
            # # if myuart.any() != 0:
                # # val = myuart.readchar()
        # # elif state == 6:
            # # if myuart.any() != 0:
                # # val = myuart.readchar()
                    
        # yield (state)                
 
       
# ----------------------------------------------------------------------

if __name__ == "__main__":
    
    print ('\033[2JInitializing controller...\n')
    
    # Below are three ques used to pass data from one task to another
    Ball_Info = task_share.Queue ('f', 10, thread_protect = False, overwrite = False,
                                  name = "Ball_Pos")
    Pos_Info = task_share.Queue ('f', 10, thread_protect = False, overwrite = False,
                                    name = "Platform_Pos")
    Speed_Info = task_share.Queue ('f', 10, thread_protect = False, overwrite = False,
                                    name = "Platform_Pos")
    
    # Task for reading the ball position from the Touch Panel.
    BallPos = cotask.Task (Ball_state_fun, name = "Ball_Status", priority = 2, period = None, profile = False, trace = False)
    # Task for reading the angle and angular speed of each axis from the encoders.
    PlatPos = cotask.Task (Platf_state_fun, name = "Platform_Status", priority = 2, period = None, profile = False, trace = False)
    # Task for performing relevant gain calculations, and updating motor PWM.
    MotorCont = cotask.Task (Motor_fun, name = "Motor_Control", priority = 2, period = None, profile = False, trace = False)
    #UI = cotask.Task (UI_fun, name = "User_Interface", priority = 1, period = None, profile = False, trace = False)
    
    # Appending tasks to the task_list for the Professor Ridgely's 'cotask.py' file.
    cotask.task_list.append (BallPos)
    cotask.task_list.append (PlatPos)
    cotask.task_list.append (MotorCont)
    #cotask.task_list.append (UI)
    
    gc.collect()
    
    vcp = pyb.USB_VCP ()
    
    # Triggering the run flag for the first task to run.
    MotorCont.go()
    while not vcp.any ():
        cotask.task_list.pri_sched ()
    vcp.read ()
    
    # Print a table of task data and a table of shared information data
    print ('\n' + str (cotask.task_list) + '\n')
    print (task_share.show_all ())
    print (BallPos.get_trace ())
    print ('\r\n')
    
    
    
    
        
