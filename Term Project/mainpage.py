## @file mainpage.py
# Documentation for / use of mainpage.py
#
# Detailed doc for mainpage.py
#
# @mainpage
#
# @section sec_port Group Portfolio
# This is my ME405 Group Portfolio. Each individual module contains collaborative
# work between Adan Martinez and Matthew Pfeiffer. This module contains links that
# gives access to Martinez's and Pfeiffer's individual work documentation. 
#
# Included modules are:
# * Lab0x01 (\ref sec_lab1)
# * Lab0x02 (\ref sec_lab2)
# * Lab0x03 (\ref sec_lab3)
# * Lab0x04 (\ref sec_lab4)
# * Lab0x05 (\ref sec_lab5)
# * Lab0x08 (\ref sec_lab8)
# * Lab0x09 (\ref sec_lab9)
#
## @section sec_lab1 Lab0x01 Documentation
# * Source:  https://bitbucket.org/mbpfeiff/me405_labs/src/master/Lab01/Lab01.py
# * Documentation: \ref Lab01.py
#            
## @section sec_lab2 Lab0x02 Documentation
# * Source:  https://bitbucket.org/mbpfeiff/me405_labs/src/master/Lab02/Lab02.py
# * Documentation: \ref Lab02.py
#
## @section sec_lab3 Lab0x03 Documentation
# * Lab03 Frontend UI Source: https://bitbucket.org/mbpfeiff/me405_labs/src/master/Lab03/Lab03.py
# * Nucleo Backend Source: https://bitbucket.org/mbpfeiff/me405_labs/src/master/Lab03/main.py
# * Note: Has not yet been documented and I couldn't figure out how to send a .csv file from the nucleo
#
## @section sec_lab4 Lab0x04 Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab04/
# * Documentation: main.py\ref\n mcp9808.py\ref\n
#
## @subsection sec_intro Introduction
# The main goal of this project is to measure the ambient temperature and 
# the STM32 Microcontroller core temperature. The ambient temperature is measure
# using a MCP9808 sensor. The pyb.I2C() allows the interface between the
# the sensor and the STM32 Microcontroller. The physical connnection is shown
# below. The microcontroller core temperature is measure
# using the pyb.ADCAll() method. The main.py file runs for 8hrs and 30 mins
# and records the time, ambient temperature, and core temperature and finally
# saves the data to a csv file. The data from the csv file is plotted
# separately.
#
# @subsection sec_notes Comments
# One big obstacle for completing this assignment was having the MCP9808 sensor
# arrive late. One team member had the MCP9808 sensor for the time being, therefore
# one member collected all the data for the sake of time. The other was in
# in charged for documentating. Other issues included the computer powering
# down and restarting collection data. The data was also plotted separately. This
# facilitated data and plot formating. 
# @subsection  sec_phycon Physical-Connection
# @image html SensorCon.png
#  
# @subsection  sec_plots  Temperature vs.Time Plot
# The plot below shows the ambient and internal temperatures as functions of time.
# This test was performed beginning at 5:53 PM in Matthew Pfeiffer's bedroom. Data
# points were taken each minute for 510 minutes.
# @image html  TempPlot.png
#
# @author Matthew Pfeiffer
# @author Adan Martinez
#
# @date Feb. 2, 2021
#
### @section sec_lab5 Lab0x05 Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab05/
# * Documentation: \ref Lab5Analysis
#
# @subsection sec_intro Introduction
# The main goal of this project is to analyze a balancing platform. The 
# mechanics of the system consists of two motors and each connected to a 
# lever arm. A pushing rod connects the lever arm to the platform. When a 
# torque is applied, the lever arm rotates, and the motion is transfer to the 
# platform via the pushing rod. For the analysis the ball is taken into 
# account. The analysis takes part by solving for kinetic equations that 
# relates the motion of the lever arm pivoting point to the base of the 
# platform. To approach the following assumptions were made. The system was 
# model as a pair of balancing beams that each move in one degree of freedom.
# We assume small angle approximation. We assume the platform has the most 
# significant mass and inertia and ignore that of the lever arm and push rod.
# For the ball we assume it rolls without slipping.  The end result is two 
# high order differential equations relating the torque from the motor, ball's
# acceleration, and angular acceleration of the platform. Also, the velocities 
# and position of the platform and lever arm.  The equations are then 
# manipulated to a matrix form. Please refer to Analysis page for further 
# detail. 
#
# @author Matthew Pfeiffer
# @author Adan Martinez
#
# @date Feb. 16, 2021
#
### @section sec_lab8 Lab0x08 Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab08/
# * Documentation: (\ref encoder.py) & (\ref MotorDriver.py)
# * HTML: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab08/html/
# * Adan M. Porfolio: https://amart426.bitbucket.io/me_405/
# * Matthew P. Porfolio: https://mbpfeiff.bitbucket.io
# @subsection sec_intro Introduction
# The main The main goal of this project was to create two drivers for 
# controlling a motor and encoder. The encoder is optical type. The encoder 
# was attached to the back of a gear that is link to the motors shaft. The 
# EncoderDriver consists of six method. The most important method is call 
# update. Which has the purpose of reading and interpreting the data. Other two 
# function consists of a method that returns the angle position in degrees and 
# the speed in degrees per seconds. On the other hand, the MotorDriver consists 
# of four methods. It has a enable and disable methods which powers on/off the 
# motor. For setting up the pins for the motors we reference the Texas 
# Instruments datasheet [1]. The motor also includes a method that sets the 
# desired duty cycle of the motors. The myCallback function only comes forth 
# when the uFAULT pin is trigger. The trigger is then restore without the need 
# of resetting the microcontroller. 
# @subsection Comments
# Some issues that our team had was triggering the nFault pin. The first method
# we use was the one suggested by our instructor which consisted in physically
# stalling the motor by holding its shaft while it rotates. This method did
# work effectively. We did not want to rely on this method since it was a bit
# dangerous when running at high rpms and we did not to damage the motor.
# Another method suggested was using a jumper while using a different pin as 
# our interrupt and see if our callback function is trigger and does its job.
#
# @subsection Ref  References
# [1]	Texas Instruments, 2019, “DRV8847 Datasheet” 
#       https://www.ti.com/lit/ds/symlink/drv8847.pdf?ts=1614963129538&ref_url=https%253A%252F%252Fwww.google.com%252F, 
#       Mar. 6 2021
#
### @section sec_lab9 Term Project Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab09/
# * Documentation: (\ref encoder.py), (\ref task_share.py), (\ref MotorDriver.py), (\ref FindPos.py), (\ref cotask.py) & (\ref main.py)
# * HTML: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab09/html/
# * Recording: https://cpslo-my.sharepoint.com/:f:/g/personal/amart426_calpoly_edu/ElURFURNPWNIsGEilCA50XkB8d3K8YR6MneihPg1AGoTSg?e=3AdpKi
# * Adan M. Porfolio: https://amart426.bitbucket.io/me_405/
# * Matthew P. Porfolio: https://mbpfeiff.bitbucket.io
#
# @subsection sec_intro Introduction
# The main goal of this project was to incorporate previously created projects
# to balance a ball on top of a platform. In other words, this project is a 
# combination for four previous projects. The first stage for developing this 
# project for balancing the ball on the platform was performing mathematical 
# analysis. This analysis allowed us to develop a state space matrix 
# (Please refer to the (\ref page1) for detail calculations). Next, we perform 
# some testing of our mathematical model using MATLAB. The system was first 
# linearized using the Jacobian method and using zero initial conditions and 
# small angle approximation where possible. We ran several simulations of our 
# model to study its behavior. The model was tested using an open loop system 
# and a closed loop system. More on this in (\ref page2). The second stage of 
# this project was to develop a MotorDriver class and encoder class for 
# controlling our motor and reading from encoders. Please refer to (\ref sec_lab8) 
# for more detail on how these drivers were developed. In addition to creating 
# this drivers, we all needed a FindPos class for reading from the touch pad. The 
# set-up and development of this Scan class is discuss in detail in Lab0x07 
# (look at each individuals ME405 Porfolio). The Scan class is composed of
# three important methods, xscan(), yscan(),zscan(), and scanall(). The first three
# are setup appropiately to read the X, Y, and Z direction when call. Scanall
# method that simultaneously scan all three directions. \n
# The final stage of this project is to combine all of the mathematical
# model and drivers to balance the platform. To approach this, our team created a main.py script
# that serves as the program where all the testing will be executed. Here is where
# our previous drivers are imported. The main.py script containts three important
# methods. The first method is called Ball_state_fun() which mainly uses the 
# the TouchControl class to obtain information about the state of motion of the ball. Another
# method called Platf_state_fun() has the main goal for obtaining information
# about the state of motion of the platform. The last method is called Motor_fun() which 
# which has the main goal for commanding the motion of the motor. This method uses the 
# information it knows about both the state of the ball and platform to calculate 
# the an appropiate Torque value to balanced the system and sets the correct duty
# cycle. In order to run this methods simultaneously we use a file called \ref cotask.py.
# This file was imported from Proffesor JR Ridgely. We also imported \ref task_share.py.
# 
# @subsection sec_bal Balancing Platform
# This shows the balancing platform with a ball on its top. Notice that the
# ball is making contact with the touch panel. There are two motors which are
# the shiny steel cylinders. There is a 1:50 gear ratio between the motor and
# encoder. The ball is made of a polymer.  
# @image html platform.jpg
# @image html platform2.jpg
#
# @subsection Results
# Our team recorded the process for balancing the ball at different scenarios. A
# link to the video recording can be access in the Recording link above.
<<<<<<< HEAD
#
# Because the IMU was not used and the rotary encoders are encremental, the platform must be
# initially leveled by hand. This introduces significant error to the system because it means
# the reference datum by the encoders is set by hand rather than by an IMU.
# 
# Beyond this, the system was can effectively balance the ball at any point on the touch panel
# when placed cautiously, without any velocity. Rolling the ball on the plate normally ends in 
# overcorrection causing futher instability and tossing the ball off. however, the system does
# respond well when perturbing the platform a small amount. This means that the angular position
# control is effective. See comments below for corrective actions that could have occured and 
# plans with more time. 
#
=======
# 
>>>>>>> c12545a7928f8064e9c92442b7b30a9ec1a99ca5
# @subsection Comments
# Our team had some issues with one of the lever arms that connects to the shaft
# of the motors. The lever arm had a clamp that was too large to fit on either shaft
# motor shaft. With this occurring, we heavely rely in one team members
# for getting better results for balancing the ball.
#
# To better improve system performance, better filtering methods could have been implemented
# on data collected. After fine tuning the gain coefficients for position, velocity, angular
# position, and angular velocity, the velocity coefficient was set to zero to completely remove
# that method of control. This was done because at low speeds, the values presented were not 
# representative of the ball velocity. A running average or low bandpass filter would have solved
# this problem and perhaps aided in dynamic control of the ball velocity. A user interface was 
# also developed using serial communication with the nucleo, but is commented out in the final
# code to increase the system performance. The goal with the user interface was to enable easy
# passing of system data from the nucleo to a frontend script that could be used to visualize it.
# With more time this user interface would have been implemented to initialize the system with the
# IMU, set the gain values, and enable easy startup and ending.
# 
# The limitting component of this project was the motor that controlled each axis motion. Unfortunately,
# the motor is slightly under-specified for the torque necessary to balance the ball with all 
# initial conditions. When the PWM duty cycle for the motors was specified over 50% for a little
# bit of time, the motors would stall. When stalled at this voltage, an over-current condition 
# occurs triggering motor shut-down to pprevent damage. Because of this, the PWM data was filtered
# before being passed to the motors so that these triggers would not be reached. Using motors more
# capable of the requisite torque values would have contributed to system performance significantly.
#
# 
#
#  
#
# @subsection Ref References
# [1]	JR Ridgely, cotask.py, task_share.py.
#       https://bitbucket.org/spluttflob/me405-support/src/master/
#       Mar. 15, 2021
# 
# @author Matthew Pfeiffer
# @author Adan Martinez
#
# @date March 15, 2021
#
##### @page page1 Analysis
#  @image html Analysis.jpeg
#  @image html Analysis1.jpeg
#  @image html Analysis2.jpeg
#  @image html Analysis3.jpeg
#  @image html Analysis4.png
#  @image html Analysis5.png
#  @image html Analysis6.png
#
############
# @page page2 Simulation
#  @subsection OpenLoopModel
#  3a) For the first simulation the ball is set initially at rest on a 
#  level platform directly above the center of gravity of the platform and 
#  there is no torque input from the motor. This simulation was run for 1 [s]. 
#  The time response of the x, theta, x_dot, and theta_dot are plotted in separate
#  plots. These plots are accurate since the ball is at its equilibrium point
#  and there is no motor torque detected. We expect the system to behave as its
#  shown.
#  @image html 3a.png
#  3b) For the second simulation, the ball is set initially at rest on a level 
#  platform offset horizontally from the center of gravity of the platform 
#  by 5 [cm] and there is no torque input from the motor. The simulation elapsed
#  for 0.4 [s]. Analyzing the plots we see that for the position(x) plot, the 
#  the ball is at a 50mm positive offset in the vertical direction. The ball
#  is release from rest and this coincides with our velocities plots(x_dot & 
#  theta_dot) which start at zero and increase with time. The angle of the
#  platform also increases due to the weigth of the ball. The ball weigth is 
#  small compare to that of the platform so it causes a small theta angle. 
#  @image html 3b.png
#  3c) The next simulation consists of having the ball initially at rest on a 
#  platform inclined at 5◦ directly above the center of gravity of the platform 
#  and there is no torque input from the motor. This simulation runs for 0.4 [s].
#  The plots shows that we start a 5deg angle position and at rest. This does
#  correlate with our prediction that the curves should have a similar time
#  response curve. 
#  @image html 3c.png
#  3d) The next simulation consisted of having the ball initially at rest on 
#  a level platform directly above the center of gravity of the platform and 
#  there is an impulse1 of 1 mNm · s applied by the motor. Run this simulation 
#  for 0.4 [s]. 
#  @image html 3d.png
#  @subsection ClodeLoopModel
#  4a) For modeling a closed loop system, we use Simulink to set up our tranfer
#  function and implemented a gain K = [-0.05 -0.02 -0.3 -0.2]. Also, the 
#  simulation in closed-loop by implementing a regulator using full state 
#  feedback. The ball is initially at rest. The simulation runs for 20[sec].
#  Our system time response is not behaving as expected. The system curves
#  should at one point approach zero 
#  @image html 4a.png
#  @image html bd.png



