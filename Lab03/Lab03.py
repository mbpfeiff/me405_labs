
"""
@author Matthew Pfeiffer
@date Feb. 2, 2021
"""

import serial
import keyboard
from matplotlib import pyplot

ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
pushed_key = None
state = 0

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

def getMessage():
    myval = ser.readline().decode('ascii')
    return myval

def on_keypress (thing):
    
    """ 
    @brief Callback which runs when the user presses a key.
    
    @details This function is run as an interrupt whenever the user presses a 
    key. This enables user input without using the non-cooperative input 
    command.
    
    @param thing Keypstroke content to be stored for Vendotron to identify.
    
    """
    global pushed_key

    pushed_key = thing.name

def InitMessage():
    print('''
          
Press [G] to ready button press...
          
          ''')
          
def ButtonPrompt():
    print('''
          
Please press the USR Button of the Nucleo to record the response...
          
          ''')

# Initialize Finite State Machine
while True:
    try:
        # State zero creates variables used in FSM
        if state == 0:
            keyboard.on_press(on_keypress)
            InitMessage()
            timebuf = []
            voltbuf = []
            mylist = []
            state = 1
        # State one prompts user to push 'G' and accepts input back.
        elif state == 1:
            if pushed_key == 'G': # Checks if a 'G' was pushed
                ser.write(str('G').encode('ascii')) #Sends G to nucleo
                myval = ser.readline().decode('ascii') #Prints nucleo welcome
                print(str(myval))
                ButtonPrompt() #Prompts the user to hit button
                pushed_key == 0
                state = 2
        # State two accepts the time and voltage buffers from the nucleo
        elif state == 2:
            runthrough = 0
            VAL = getMessage() # Reads from the nucleo
            if VAL != '': # Checks if the nucleo sent anything
                mylist = VAL.strip('\n').split(', ') # Formats and places
                timebuf.append(int(mylist[0]))       # buffers into appropriate
                voltbuf.append(float(mylist[1]))     # lists to be plotted.
                if len(timebuf) == 1000:
                    state = 3
        # State three plots the buffer data from the nucleo and ends the FSM
        elif state == 3:
            print('''
SEE PLOT TAB
                  ''')
            pyplot.figure
            pyplot.plot(timebuf,voltbuf)
            pyplot.title('Button Press Response Curve')
            pyplot.xlabel('Time [\u03BCs]')
            pyplot.ylabel('Voltage [V]')
            ser.close()
            break
            
                
    except:
        ser.close()
        break