
import pyb
from pyb import UART
import array

myuart = UART(2)
state = 0
itsit = 0

def ButtonPressed(Which_Pin):
    '''
    @brief Callback for when the button is pushed
    @details When the button is pushed an external interrupt is linked
    to this callback. The ButtonHit flag is raised for the FSM.
    '''
    global ButtonHit
    ButtonHit = True
    
def ReadIt(pre_buffy):
    '''        
    @brief Checks the recorded adc values for the appropriate measurement time.
    @details checks if the adc values stored in the small buffer
    are appropriate. If so, global variable "itsit" is raised for
    the FSM to handle appropriately.
    '''
    global itsit
    itsit = 0
    if pre_buffy[1] < 10:
        if pre_buffy[-1] > 4080:
            itsit = 1
          

           
# Initializes FSM
while True:
    try:
        if state == 0:
            myButton = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)          # Initializes Button Pin
            ButtonInt = pyb.ExtInt(myButton, pyb.ExtInt.IRQ_FALLING,    # Initializes Button Interrupt
                            pyb.Pin.PULL_NONE, callback=ButtonPressed)
            ButtonHit = False
            pre_buffy = array.array ('H',[0 for index in range(1000)])  # Create arrays for adc check, 
            voltbuffy = array.array ('f', (0 for index in range(1000))) # voltage buffer, and time buffer
            timebuffy = array.array ('H', (0 for index in range(1000)))
            myTim = pyb.Timer(6, freq=100000, callback=None)            # Initializes Timer 6 to 100000Hz
            myPinout =  pyb.Pin(pyb.Pin.board.PA0)                      # Initializes Button output pin
            adc = pyb.ADC(myPinout)                                     # Initializes adc pin
            state = 1                                                   # Transitions to state 1
        elif state == 1:
             if myuart.any() != 0:                                      # Checks if anything has been
                 val = myuart.readchar()                                # sent to the nucleo
                 myuart.write("You have sent an ASCII " + str(val) + ' to the Nucleo\n')
                 if val == 71:                                          # If it was a 'G', transition
                     state = 2                                          # to the next state
        elif state == 2:
            if ButtonHit == True:                                       # If the button is hit, adc values
                if itsit == 0:                                          # stored in a small buffer and 
                    adc.read_timed(pre_buffy, myTim)                    # checked to ensure they contain
                    ReadIt(pre_buffy)                                   # relevent values. If not, adc is read
                else:                                                   # again and values are checked again.
                    for val in range(1000):                             # If they are good values, the integer
                        voltbuffy[val] = float(pre_buffy[val])*3.3/4096 # ADC counts are converted to voltage
                        ButtonHit = False                               # and the time buffer is created
                    for val in range(1000):
                        timebuffy[val] = (val+1)
                    itsit = 0
                    state = 3                                           # Transition to state 3
                                        
        elif state == 3:
            for val in range(1000):                                     # The time buffer and voltage buffer
                myuart.write(str(timebuffy[val]) + ', ' + str(voltbuffy[val]) + '\n') # are sent to the frontend
            state = 1                                                   # for plotting one line at a time.
    except:      # If any errors occur, the loop is broken.
        break