''' 
@file Lab01.py

@brief Functional vending machine that accepts user input

@details Vendotron is the software for a vending machine that uses the 
terminal as a user interface. Vendotron accepts pennies, nickels, quarters, 
ones, fives, tens, and twenties, and gives four drink selections at different 
costs. When a drink is selected, vendotron ensures appropriate funds have been
given, dispenses the drink, and returns the change in the fewewst denominations
possible. If insufficient funds were entered, Vendotron prints an insufficient
funds message in the user interface and returns to the main menu and balance
display.

@author Matthew Pfeiffer

@date Jan. 18, 2021
'''

## @brief the state variable identifies the current state.
#  @details The state variable is used to transition from one state to the 
#  next and identifies the current state.
state = 0

import keyboard
## @brief pushed_key will hold the contents of a keystroke.
#  @details pushed_key will be used to store the contents of a keystroke and 
#  and to check those comments in the body of the script.
pushed_key = None

def getChange(price, payment) :
    
   '''
   @brief Computes change for monetary transaction.
   
   @details Computes the change to be returned after given a set price as 
   an integer number of pennies and a payment as an integer number of pennies. 
   If the payment is sufficient, the appropriate change is returned as a tuple 
   with the fewest denominations possible. If the funds are insufficient, 
   None is returned. If exact change is entered, 'Nochange' is returned.
   
   @param price The price of the item as an integer number of pennies.
    
   @param payment The price of the item as a tuple of acceptable 
   denominations (pennies,nickels,dimes,quarters,ones,fives,tens,twenties)
    
   @return If the funds are sufficient, a tuple of the change with the 
   fewest denominations possible is returned in the format (pennies,nickels,
   dimes,quarters,ones,fives,tens,twenties). Otherwise, the funciton 
   returns None.

    
   '''
   # Check that datatypes are appropritate. If not return (None)

   change = payment - price # change calculates the change to be returned as
   # an integer number of pennies. 
   
   changelist = [] # Creates a mutable list with unspecified values.
   
   if change > 0: # Sets a condition that change must be necessary.
     
     # Here, if the denomination is suitable for change, the number of 
     # bills/coins requisite is determined and the integer number of values
     # is subtracted from the integer value of change to reflect the 
     # deduction.
     
     changelist.append(int(change/2000))       
     change = change - int(change/2000)*2000
    
     changelist.append(int(change/1000))
     change = change - int(change/1000)*1000
    
     changelist.append(int(change/500))
     change = change - int(change/500)*500

     changelist.append(int(change/100))
     change = change - int(change/100)*100
    
     changelist.append(int(change/25))
     change = change - int(change/25)*25

     changelist.append(int(change/10))
     change = change - int(change/10)*10
 
     changelist.append(int(change/5))
     change = change - int(change/5)*5
     
     changelist.append(int(change/1))
     change = change - int(change)
 
     changelist.reverse() # Because denominations appended in reverse
     return(tuple(changelist)) # Returns immutable tuple of bills to return
 
   elif change == 0:
       return('Nochange')
       
 
   else: # If the change is < 0, insufficient funds. Return None1111C
       return(None)

def printWelcome():
    '''
    @brief This function prints the welcome screen.
    
    @details This function lists the welcome menu with a list of the different 
    drink choices, costs, and an updated balance.
    '''
    print('''       
          
          
          Cuke [C]         $1.00
          Popsi [P]        $1.20
          Spryte [S]       $0.85
          Dr. Pupper [D]   $1.10
            
          Balance          $''' + '%.2f' %(Balance/100) + '''
          
          Eject [E]''')

          
def cukeDispense():
    
    '''
    @brief This function prints an indication that Cuke was dispensed.
    
    @details This function is used to indicate to the user that Cuke was 
    dispensed if funds were sufficient and the 'Cuke' button was pushed.
    '''
    
    print('''
          
          
          
          
          
          <Cuke Dispensed> ''')
          
def popsiDispense():
    
    '''
    @brief This function prints an indication that Popsi was dispensed.
    
    @details This function is used to indicate to the user that Popsi was 
    dispensed if funds were sufficient and the 'Popsi' button was pushed.
    '''
    
    print('''
          
          
          
          
          
          <Popsi Dispensed>''')
    
def spryteDispense():
    
    '''
    @brief This function prints an indication that Spryte was dispensed.
    
    @details This function is used to indicate to the user that Spryte was 
    dispensed if funds were sufficient and the 'Spryte' button was pushed.
    '''
    
    print('''
          
          
          
          
          
          <Spryte Dispensed>''')
          
def pupperDispense():
    
    '''
    @brief This function prints an indication that Dr. Pupper was dispensed.
    
    @details This function is used to indicate to the user that Dr. Pupper was 
    dispensed if funds were sufficient and the 'Dr. Pupper' button was pushed.
    '''
    
    print('''
          
          
          
          
          
          <Dr. Pupper Dispensed>''')

def insufficient():
    
    '''
    @brief This function prints 'Insufficient Funds...'
    
    @details This function is used to indicate that there are insufficient 
    funds for the current selection.
    '''
    
    print('''
          
          
          
          

          Insufficient Funds...''')
          
def on_keypress (thing):
    
    """ 
    @brief Callback which runs when the user presses a key.
    
    @details This function is run as an interrupt whenever the user presses a 
    key. This enables user input without using the non-cooperative input 
    command.
    
    @param thing Keypstroke content to be stored for Vendotron to identify.
    
    """
    global pushed_key

    pushed_key = thing.name

keyboard.on_press (on_keypress)  ########## Set callback

# Initiates the loop for the finite statem machine. Runs until user ends.
while True :
    try: 
        if state == 0:
            
            ## @brief Current balance in an integer number of pennies.
            #  @details Balance is an integer number of pennies presently 
            #  currently entered by the user. To be used to determine if funds
            #  are sufficient for a drink and when returning change.
            Balance = 0
            
            ## @brief Cuke variable is the cost of Cuke in pennies.
            #  @details The Cuke variable uses an integer number of pennies to 
            #  represent the cost of a Cuke drink.
            Cuke = 100
            
            ## @brief The cukeflg variable is a signal that Cuke was pushed.
            #  @details The cukeflg variable signals that the Cuke button on
            #  the vending machine was pushed for proper drink dispense.
            cukeflg = 0
            
            ## @brief Popsi variable is the cost of Popsi in pennies.
            #  @details The Popsi variable uses an integer number of pennies to 
            #  represent the cost of a Popsi drink.
            Popsi = 120
            
            ## @brief The popsiflg variable is a signal that Popsi was pushed.
            #  @details The popsiflg variable signals that the Popsi button on
            #  the vending machine was pushed for proper drink dispense.
            popsiflg = 0
            
            ## @brief Spryte variable is the cost of one Spryte in pennies.
            #  @details The Spryte variable uses an integer number of pennies to 
            #  represent the cost of a Spryte drink.
            Spryte = 85

            ## @brief The spryteflg variable is a signal that spryte was pushed.
            #  @details The spryteflg variable signals that the spryte button on
            #  the vending machine was pushed for proper drink dispense.
            spryteflg = 0
            
            ## @brief Pupper variable is the cost of Dr. Pupper in pennies.
            #  @details The Dr. Pupper variable uses an integer number of 
            #  pennies to represent the cost of a Cuke drink.
            Pupper = 110
            
            ## @brief The pupperflg variable is a signal that Dr. Pupper was pushed.
            #  @details The Pupperflg variable signals that the Dr. Pupper button on
            #  the vending machine was pushed for proper drink dispense.
            pupperflg = 0
            delay = 10000
            printWelcome()
            state = 1
        
        elif state == 1:
            if pushed_key == '0': # Assigns correct monetary value to Balance
                Balance += 1
                printWelcome()
                pushed_key = None
            elif pushed_key == '1':
                Balance += 5
                printWelcome()
                pushed_key = None
            elif pushed_key == '2':
                Balance += 10
                printWelcome()
                pushed_key = None
            elif pushed_key == '3':
                Balance += 25
                printWelcome()
                pushed_key = None
            elif pushed_key == '4':
                Balance += 100
                printWelcome()
                pushed_key = None
            elif pushed_key == '5':
                Balance += 500
                printWelcome()
                pushed_key = None
            elif pushed_key == '6':
                Balance += 1000
                printWelcome()
                pushed_key = None
            elif pushed_key == '7':
                Balance += 2000  
                printWelcome()
                pushed_key = None
            elif pushed_key == 'e': # Transitions to return state if ejecct hit
                state = 2
                pushed_key = None
            elif pushed_key == 'c': # Raises cukeflg if c hit
                cukeflg = 1
                state = 2
                pushed_key = None
            elif pushed_key == 'p': # Raises popsiflg if p hit
                popsiflg = 1
                state = 2
                pushed_key = None
            elif pushed_key == 's': # Raises spryteflg if s hit
                spryteflg = 1
                state = 2
                pushed_key = None
            elif pushed_key == 'd': # Raises pupperflg if d hit
                pupperflg = 1
                state = 2
                pushed_key = None
                

            
        elif state == 2:
            # Checks flags if and decides if appropriate change entered.
            # If not, drops to insufficient funds state.
            # If so, dispenses drink, returns change and sets back to state 1.
            if cukeflg == 1:
                cukeflg = 0
                if getChange(100, Balance) == None:
                    state = 3
                elif getChange(100,Balance) == 'Nochange' :
                    cukeDispense()
                    Balance = 0
                    printWelcome()
                    state = 1                    
                else :
                    cukeDispense()
                    print(''' 

                          
          Returned ''' + str(getChange(100, Balance)))
                    Balance = 0
                    printWelcome()
                    state = 1
                    
            elif popsiflg == 1:
                popsiflg = 0
                if getChange(120, Balance) == None:
                    state = 3
                elif getChange(120, Balance) == 'Nochange' :
                    popsiDispense()
                    Balance = 0
                    printWelcome()
                    state = 1
                else :
                    popsiDispense()
                    print(''' 
                          
                          
          Returned ''' + str(getChange(120, Balance)))
                    Balance = 0
                    printWelcome()
                    state = 1
                    
            elif spryteflg == 1:
                spryteflg = 0
                if getChange(85, Balance) == None:
                    state = 3
                elif getChange(85, Balance) == 'Nochange' :
                    spryteDispense()
                    Balance = 0
                    printWelcome()
                    state = 1
                else :
                    spryteDispense()
                    print(''' 
                          
                          
          Returned ''' + str(getChange(85, Balance)))
                    Balance = 0
                    printWelcome()
                    state = 1
                    
            elif pupperflg == 1:
                pupperflg = 0
                if getChange (110, Balance) == None:
                    state = 3
                elif getChange(110, Balance) == 'Nochange' :
                    pupperDispense()
                    Balance = 0
                    printWelcome()
                    state = 1
                else :
                    pupperDispense()
                    print('''
                          
                          
          Returned ''' + str(getChange(110, Balance)))
                    Balance = 0
                    printWelcome()
                    state = 1
            else :
                if getChange(0, Balance) == 'Nochange' :
                    Balance = 0
                    printWelcome()
                    state = 1
                else:
                    
                    print('''
                          
                          
          Returned ''' + str(getChange(0, Balance)))
                    Balance = 0
                    printWelcome()
                    state = 1
                
        elif state == 3:
            # Prints insufficient funds message and then returns back to state 1
            insufficient()
            printWelcome()
            state = 1
        
    # If keyboard interrupt is performed print user ended message and then
    # Break the loop.
    except KeyboardInterrupt:
        print('''
          Program Ended by User (ctrl+c) Pressedpsdc47p7d7s7c ''')
        break
