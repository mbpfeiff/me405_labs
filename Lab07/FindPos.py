'''
@file Findpos.py

\brief      Acts as a driver for the touch panel device.
@details    The resistive touch-panel is modeled as four resistors connected
            at a central node. By toggling activation mode of each of these 
            resistors, the touch panel activity and coordinate readings can
            be observed. This file contains a class declaration for TouchControl,
            which users can access by importing into another script. Within the
            touchControl class exist four seperate methods. One that reads the 
            individual activation of the touch panel, one for the x-coordinate
            position, and one for the y-coordinate position. the final method
            reads all three values at once and returns them as a tuple 

@author Matthew Pfeiffer
@date Mar. 2, 2021
'''

import pyb
import utime
import array
from pyb import Pin
from pyb import ADC

class TouchControl:
    """
    @brief              TouchControl class holds relevant methods for touch-panel use.
    @details            The TouchControl class has four methods. Three of which 
                        individually scan the respective axes (x, y, and z), and one 
                        which scans all three at once and returns a tuple containing
                        all three values.
    """
    def __init__(self,xm,xp,ym,yp,xlength,ywidth,CoordCent):
        """
        @brief          Constructor for Class TouchControl.
        @details        This constructor expects an object of the TouchControl class to
                        be defined with four arbitrary pins each already defined as pin 
                        objects. One for xm, xp, ym, and yp. Also, this constructor 
                        expects declaration of the total length, width, and coordinate
                        centerpoint of the touch panel.
        """
        self.xm = xm
        self.xp = xp
        self.ym = ym
        self.yp = yp
        self.xlength = xlength
        self.ywidth = ywidth   
        self.CoordCent = CoordCent        
    
    @micropython.native    
    def scanall(self): 
        """
        @brief          This method scans x-position, y-position, and z-position.
        @details        This method scans all three positions at once in under 1500 [us]
                        and returns them as a tuple of the form (x-coord, y-coord, z-active)
                        The z-value represents a signal to whether the touch panel is active
                        or not. When z is 0, the touch-panel is active, otherwise it is 
                        inactive. This means that the coordinate positions for x and y have
                        no meaning when z is 1. Each value is read three times and averaged
                        to filter the noise associated with readings. The z value is 
                        attenuated to either a 1 or a 0 for logical boolian use.
        """        
        self.xm = Pin(self.xm, mode=Pin.IN)
        
        self.xp = Pin(self.xp, mode=Pin.IN)

        ADC_xm = ADC(self.xm)

        self.xm.init(mode=Pin.ANALOG)
        self.yp.init(mode=Pin.OUT_PP, value=1)
        self.ym.init(mode=Pin.OUT_PP, value=0)

        ADC_xm = ADC(self.xm)
        aveyval = (ADC_xm.read()+ADC_xm.read()+ADC_xm.read())/3 
        Ypos = self.ywidth*(aveyval/4096)-self.CoordCent[1]
        
        self.ym = Pin(self.ym, mode=Pin.IN)

        self.yp = Pin(self.yp, mode=Pin.IN)
        ADC_ym = ADC(self.ym)

        self.xm.init(mode=Pin.OUT_PP, value=0)
        self.xp.init(mode=Pin.OUT_PP, value=1)
        self.ym.init(mode=Pin.ANALOG)       
        
        ADC_ym = ADC(self.ym)
        avexval = (ADC_ym.read()+ADC_ym.read()+ADC_ym.read())/3
        Xpos = self.xlength*(avexval/4096)-self.CoordCent[0]
        
        self.xp = Pin(self.xp, mode=Pin.IN)

        self.yp.init(mode=Pin.OUT_PP, value=1)

        ADC_ym = ADC(self.ym)
        
        avezval = (ADC_ym.read()+ADC_ym.read()+ADC_ym.read())/3
        Zpos = int((avezval/4096)/.99)

        
        return(Xpos,Ypos,Zpos)
    
    @micropython.native
    def xscan(self):
        """
        @brief          This function finds the x position of touch panel. 
        @details        The xscan function sets xplus to high, xminus to ground,
                        and defines yminus as an ADC object to get a reading of
                        the x-position as an adc count ratio of the total panel
                        length. The ADC is read three times and averages the 
                        three values to develop a moving average filter, reducing
                        the associated noise.
        """
        self.ym = Pin(self.ym, mode=Pin.IN)

        self.yp = Pin(self.yp, mode=Pin.IN)
        ADC_ym = ADC(self.ym)

        self.xm.init(mode=Pin.OUT_PP, value=0)
        self.xp.init(mode=Pin.OUT_PP, value=1)
        self.ym.init(mode=Pin.ANALOG)
        ADC_ym = ADC(self.ym)
        aveval = (ADC_ym.read()+ADC_ym.read()+ADC_ym.read())/3        
        return (self.xlength*aveval/4096)-self.CoordCent[0]

    @micropython.native
    def yscan(self):
        """
        @brief          This function finds the y-position of the touch panel.
        @details        The yscan function sets yplus to high, yminus to ground,
                        and defines xminus as an adc object to get a reading of
                        the y-position as an adc count ratio of the total panel
                        length. The y value is read three times and averaged to 
                        associate a moving-average filter that reduces the noise.
        """
        self.xm = Pin(self.xm, mode=Pin.IN)
        
        self.xp = Pin(self.xp, mode=Pin.IN)

        ADC_xm = ADC(self.xm)

        self.xm.init(mode=Pin.ANALOG)
        self.yp.init(mode=Pin.OUT_PP, value=1)
        self.ym.init(mode=Pin.OUT_PP, value=0)

        ADC_xm = ADC(self.xm)
        aveval = (ADC_xm.read()+ADC_xm.read()+ADC_xm.read())/3
        return self.ywidth*(aveval/4096)-self.CoordCent[1]

    @micropython.native
    def zscan(self):
        """
        @brief          This function determines if the panel is being touched.
        @details        The zscan function sets yplus to high, xminus to ground,
                        and defines yminus as an adc object to determine whether
                        or not the panel is being touched. The z-value is read 
                        three times and averaged in order to develop a moving-average
                        filter to reduce the noise. This value is then attenuated to 
                        either a 1 or a 0 for boolian logic use. 1 Represents an 
                        inactive touch panel, rendering x and y values inaccurate.
        """                
        self.ym = Pin(self.ym, mode=Pin.IN)
        
        self.xp = Pin(self.xp, mode=Pin.IN)

        ADC_ym = ADC(self.ym)

        self.xm.init(mode=Pin.OUT_PP, value=0)
        self.ym.init(mode=Pin.ANALOG)
        self.yp.init(mode=Pin.OUT_PP, value=1)

        ADC_ym = ADC(self.ym)
        aveval = (ADC_ym.read()+ADC_ym.read()+ADC_ym.read())/3
        return int((aveval/4096)/.99)

if __name__ == "__main__":
    A6 = Pin(Pin.cpu.A6)
    A0 = Pin(Pin.cpu.A0)
    A7 = Pin(Pin.cpu.A7)
    A1 = Pin(Pin.cpu.A1)
    Length = 176
    Width = 100
    CoordCent = (Length/2,Width/2)
    TouchScreen = TouchControl(A6,A0,A7,A1,Length,Width,CoordCent) 
    timestore = []
    with open("Scanall.csv","w") as dataout:
         dataout.write('xpos [mm]' + ', ' + 'ypos [mm]' + ', ' +
                       'avtive [0=yes]' + ',\n')
    for n in range(100):
        starttime = utime.ticks_us()
        saveval = TouchScreen.scanall()
        endtime = utime.ticks_us()
        #timestore.append(utime.ticks_diff(endtime,starttime))
        with open("Scanall.csv","a") as dataout:
            dataout.write("{:.2f}".format(saveval[0]) + 
                          ', ' + "{:.2f}".format(saveval[1]) + 
                          ', ' + "{:.2f}".format(saveval[2]) + ',\n')
    avetime = sum(timestore)/len(timestore)
    print(avetime)