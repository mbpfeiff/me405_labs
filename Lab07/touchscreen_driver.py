import pyb
from pyb import Pin
from pyb import ADC

PIN_xp = Pin(Pin.cpu.A0)
PIN_xm = Pin(Pin.cpu.A6)
ADC_ym = ADC(Pin.cpu.A1)
PIN_ym = Pin(Pin.cpu.A1, mode=Pin.IN)
PIN_xm.init(mode=Pin.OUT_PP, value=0)
PIN_xp.init(mode=Pin.OUT_PP, value=1)
PIN_ym.init(mode=Pin.ANALOG)
ADC_ym = ADC(PIN_ym)
ADC_ym.read()
while True:
	print(ADC.read())
