# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 11:20:37 2021

@author: matth
"""

from matplotlib import pyplot

xbuf = []
ybuf = []
zbuf = []
mylist = []
with open("Scanall.csv", "r") as tempout:
    for n in range(100):
        row = tempout.readline()
        if n > 1:
            mylist = row.strip('\n').split(',')
            xbuf.append(float(mylist[0]))
            ybuf.append(float(mylist[1]))
            zbuf.append(float(mylist[2]))
sequencebuf = []
for n in range(100):
    if n>1:
        sequencebuf.append(n)
        
pyplot.figure
pyplot.plot(sequencebuf,xbuf)
pyplot.legend(['Sequence Number','XPosition'])
pyplot.ylim(0,85)
pyplot.xlim(50,100)
pyplot.title('Ambient and Core Temperature')
pyplot.xlabel('Sequence Number')
pyplot.ylabel('X Coordinate Position [mm]' )