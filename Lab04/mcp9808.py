'''
@file mcp9808

@author Matthew Pfeiffer
@author Adan Martinez-Cruz

@date Feb. 2, 2021
'''




def init(sensor, addr):
    import pyb
    import array
    from pyb import I2C
    import utime
    
    global address
    global i2c
    
    i2c = sensor
    i2c.init(I2C.MASTER,baudrate=115273)
    address = addr
    
def check():
    validaddr = i2c.scan()
    if validaddr[0] == address:
        return(True)

def celsius():
    import array
    databytes = array.array('B',[0 for index in range(2)])
    i2c.mem_read(databytes,address,5)
    MSB = databytes[0]&31
    LSB = databytes[1]
    if MSB&16 == 16:
        temp = 256 - (MSB*16 + LSB/16)
        return temp
    else:
        temp = MSB*16 + LSB/16
        return temp
        
def fahrenheit():
    import array
    databytes = array.array('B',[0 for index in range(2)])
    i2c.mem_read(databytes,0x18,5)
    MSB = databytes[0]&31
    LSB = databytes[1]
    if MSB&16 == 16:
        temp = (256 - (MSB*16 + LSB/16))*(9/5)+32
        return temp
    else:
        temp = (MSB*16 + LSB/16)*(9/5)+32
        return temp

if __name__ == "__main__":
    i2c = pyb.I2C(1)
    databytes = array.array('B',[0 for index in range(2)])
    while True:
        try: 
            print(str(celsius()) + ' [C]')
            print(str(fahrenheit()) + ' [F]')
            utime.sleep(1)
        except KeyboardInterrupt:
            print('Program Ended By User')
            break

