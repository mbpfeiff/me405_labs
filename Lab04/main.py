'''
@file main.py

@author Matthew Pfeiffer
@author Adan Cruz-Martinez

@date Feb. 4, 2021
'''

import pyb
import array
import utime
import coretemp as ins
import mcp9808 as amb
from pyb import I2C



state = 0

while True:
    try:
        if state == 0:
            i2c = pyb.I2C(1)
            addr = 24
            amb.init(i2c,addr)
            if amb.check() == True:
                state = 1
                pernum = 0
                starttime = utime.ticks_ms()
                time = 0
                period = 0
                stoval = []
                with open("TempOutput.csv","w") as tempout:
                    tempout.write('Time [min], Ambient Temperature [F], Core_Temperature[F],\n\n')
        elif state == 1:
            while (time/60000) <= 510: 
                if utime.ticks_ms()-starttime == 0:
                    if pernum != 0:
                        period = time*pernum
                        pernum += 1
                    else:
                        pernum = 1
                time = period+utime.ticks_ms()-starttime 
                with open("TempOutput.csv", "a") as tempout:
                    tempout.write("{:.2f}".format((time)/60000) 
                                  + ', ' + "{:.2f}".format(amb.fahrenheit()) + ', ' 
                                  + "{:.2f}".format(ins.readtemp()) + ',\n')
                    utime.sleep(60)
            print('\nAll Complete :)\n')
            break
            
    except KeyboardInterrupt:
        print('\nProgram Ended By User\n')
        break