'''
@file Lab02.py

@brief Lab02 script tests reaction time.

@details Lab 02 scrippt tests user reaction time by displaying by switching on and off 
the LD2 light of the Nucleo every 2.5 seconds. When the light turns ont he user is 
expected to hit the USR button as quickly as possible. When pushed, the Nucleo prints
the time for that light and appends the value for a calculation of average time. When 
"ctrl+C" is hit, the program ends and the average time is displayed in milliseconds.

@author Matthew Pfeiffer

@date Jan. 25, 2021
'''
import pyb
import micropython

micropython.alloc_emergency_exception_buf(200)

##@brief  The state variable controls the Finite State Machine.
# @details The state variable controls the Finites State Machine by acting as a 
# transition variable when transition is necessary.
state = 0
##@brief The ButtonHit variable is used to mark when the button is hit.
# @details The ButtonHit variable is set in the ButtonPress ISR to act as a flag
# for the finite state machine to know that the button was hit.
ButtonHit = 0
##@brief The isr_gain variable acts as a timer count in ms when the LED is on.
# @details The isr_gain variable is initially set to zero and counts time in 
# microseconds until the button is hit when the LED is on.
isr_gain = 0
##@brief The measure List provides storage for each of the reaction times.
# @details The isr_gain value at the time of button pressing is appended to 
# the Measure list. When the program is ended, the Measure list is used to 
# create an average of all the reaction time values.
Measure = []
##@brief HitAgain boolian is a flag to to prevent multiple USR button pushes. 
# @details HitAgain is cleared every LED ISR callback to signal that the button 
# can be pushed. When the button is pushed the boolian is set to 1 to signal 
# that no more measurement values should be stored in Measure[].
HitAgain = 0

def LEDCallback(Timsource):
    '''
    @brief LEDCallback() Performs the LED Timer ISR
    @details LEDCallback() performs the interrupt service routine for the LED control
    timer 2. When called every 2.5 seconds, the LED value is switched between either
    on or off. The global variable HitAgain is reset on each LED switch to ensure the
    user cannot hit the USR button more than once during each LED on time period.
    '''

    myPin.value(1-myPin.value())
    global HitAgain
    HitAgain = 0
    
def ButtonPress(which_pin):
    '''
    @brief ButtonPress function performs the ISR callbackfor the button being pushed.
    @details ButtonPress function flags the ButtonHit boolian when triggered by an
    external interrupt because the button was pushed.
    '''
    global ButtonHit
    ButtonHit = 1    

def MeasureCallback(ISR_MEASURE):
    '''
    @brief MeasureCallback Function performs ISR for the Reaction Measurement Timer.
    @details MeasureCallback function increments isr_gain each millisecond when the
    light is on to get an accurate measure of when the button was pushed. This value
    is then displayed and stored in measure[].
    '''
    global isr_gain
    isr_gain += 1
    
        
while True:
    try:
        if state == 0:
            myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)   
            myButton = pyb.Pin(pyb.Pin.cpu.C13)
            LEDTimer = pyb.Timer(2, period=199999999, prescaler=0, callback=LEDCallback)
            
            ButtonInterrupt = pyb.ExtInt(myButton, mode=pyb.ExtInt.IRQ_FALLING,
                                  pull=pyb.Pin.PULL_NONE, callback=ButtonPress)  
                                  
            myTim = pyb.Timer(5, period=999, prescaler=79, callback=MeasureCallback)                                  
            state = 1 
        elif state == 1:
            if myPin.value() == 0:
                myTim.callback(None)
                isr_gain = 0
            else:
                myTim.callback(MeasureCallback)
                state = 2
        elif state == 2:    
            if ButtonHit != 0:
                if HitAgain == 0:
                    myTim.callback(None)
                    Measure.append(isr_gain)
                    print('\nTime Was ' + str(1000*isr_gain) + '[ms] \n')
                    ButtonHit = 0
                    HitAgain = 1
                    isr_gain = 0
                    state = 1
                else:
                    state = 1
                
            else:
                state = 1
    except KeyboardInterrupt:
        LEDTimer.callback(None)
        myTim.callback(None)
        AVETIME = 1000*(sum(Measure)/len(Measure))
        print('\nAverage Time = ' + str(AVETIME) + ' [ms] \n')
        break